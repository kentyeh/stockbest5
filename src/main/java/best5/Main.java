package best5;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.http.Consts;
import org.apache.http.HttpHost;
import org.apache.http.client.CookieStore;
import org.apache.http.client.config.AuthSchemes;
import org.apache.http.client.config.CookieSpecs;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.config.ConnectionConfig;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.config.SocketConfig;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;

/**
 *
 * @author Kent Yeh
 */
public class Main {

    private static final Logger log = Logger.getLogger(Main.class.getName());

    public static void main(String[] args) throws IOException, NoSuchAlgorithmException, KeyStoreException, KeyManagementException {
        if (args.length == 0) {
            log.log(Level.INFO, "請輸入股票代號(code1 code2 code3 ...)或 REGENDB 參數重新取得股票代號");
        }
        org.apache.http.conn.ssl.SSLContextBuilder builder = new org.apache.http.conn.ssl.SSLContextBuilder();
        builder.loadTrustMaterial(null, new TrustStrategy() {
            @Override
            public boolean isTrusted(X509Certificate[] xcs, String string) throws CertificateException {
                return true;
            }
        });
        SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(builder.useProtocol("TLSv1.2").build());
        Registry<ConnectionSocketFactory> socketFactoryRegistry = RegistryBuilder.<ConnectionSocketFactory>create()
                .register("http", PlainConnectionSocketFactory.INSTANCE)
                .register("https", sslsf)
                .build();
        CookieStore cookieStore = new BasicCookieStore();
        RequestConfig defaultRequestConfig = RequestConfig.custom()
                .setCookieSpec(CookieSpecs.BEST_MATCH)
                .setExpectContinueEnabled(true)
                .setStaleConnectionCheckEnabled(true)
                .setTargetPreferredAuthSchemes(Arrays.asList(AuthSchemes.NTLM, AuthSchemes.DIGEST))
                .setProxyPreferredAuthSchemes(Arrays.asList(AuthSchemes.BASIC))
                .build();
        PoolingHttpClientConnectionManager cm = new PoolingHttpClientConnectionManager(socketFactoryRegistry);
        SocketConfig socketConfig = SocketConfig.custom().setTcpNoDelay(true).setSoKeepAlive(true).build();
        cm.setDefaultSocketConfig(socketConfig);
        cm.setSocketConfig(new HttpHost(args.length == 1 && "regendb".equalsIgnoreCase(args[0]) ? "isin.twse.com.tw" : "mis.twse.com.tw", 443), socketConfig);
        ConnectionConfig connectionConfig = ConnectionConfig.custom().setCharset(Consts.UTF_8).build();
        cm.setDefaultConnectionConfig(connectionConfig);
        cm.setConnectionConfig(new HttpHost(args.length == 1 && "regendb".equalsIgnoreCase(args[0]) ? "isin.twse.com.tw" : "mis.twse.com.tw", 443), ConnectionConfig.DEFAULT);
        try (CloseableHttpClient httpclient = HttpClients.custom().setConnectionManager(cm).setDefaultCookieStore(cookieStore)
                .setDefaultRequestConfig(defaultRequestConfig).build()) {
            if (args.length == 1 && "regendb".equalsIgnoreCase(args[0])) {
                CodeDB.getInstance(httpclient).reGenerate();
            } else {
                for (Item item : new Hunter(httpclient).hunt(args)) {
                    System.out.println(item.toString());
                }
            }
        } finally {
            cm.shutdown();
        }
    }
}

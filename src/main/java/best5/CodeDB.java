package best5;

import java.io.IOException;
import java.io.InvalidObjectException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.cyberneko.html.parsers.DOMParser;
import org.skife.jdbi.v2.DBI;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.SqlBatch;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.sqlite.SQLiteConfig;
import org.sqlite.SQLiteDataSource;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author Kent Yeh
 */
public final class CodeDB implements Runnable {

    private static final Logger log = Logger.getLogger(CodeDB.class.getName());
    private SQLiteDataSource ds = null;
    private static CodeDB DEFAULT = null;
    private CloseableHttpClient hc = null;
    private final XPath xpath = XPathFactory.newInstance().newXPath();
    private boolean x86 = false;

    public static CodeDB getInstance(CloseableHttpClient hc) {
        if (DEFAULT == null) {
            DEFAULT = new CodeDB(hc);
        }
        return DEFAULT;
    }

    private CodeDB(CloseableHttpClient hc) {
        this.hc = hc;
        if (ds == null) {
            if (Item.isWindows) {
                x86 = "x86".equals(System.getProperty("os.arch"));
                String sysdir = System.getenv("WINDIR") + "\\" + (x86 ? "System32" : "SysWOW64");
                Path sql = Paths.get(sysdir, "sqlite3.dll");
                if (!Files.exists(sql)) {
                    throw new RuntimeException("未安裝Sqlite，請先到https://sqlite.org/download.html下載"
                            + (x86 ? "x86" : "x64") + "版本，並解壓縮到" + sysdir);
                }
            }
            SQLiteConfig config = new SQLiteConfig();
            config.setSharedCache(true);
            config.enableRecursiveTriggers(true);
            ds = new SQLiteDataSource(config);
            Path cp = Paths.get(".", "stock.db");
            ds.setUrl("jdbc:sqlite:" + cp.toAbsolutePath().normalize().toString());
            try (Dao dao = new DBI(ds).open(Dao.class)) {
                String tabname = dao.stockTabName();
                if (tabname == null || tabname.isEmpty()) {
                    dao.createTable();
                    run();
                }
            }
        }
    }

    public void reGenerate() {
        try (Dao dao = new DBI(ds).open(Dao.class)) {
            String tabname = dao.stockTabName();
            if (tabname != null && !tabname.isEmpty()) {
                log.log(Level.INFO, "移除資料庫資料");
                dao.removeAll();
            }
            run();
        }
    }

    public String findSymbol(String code) {
        try (Dao dao = new DBI(ds).open(Dao.class)) {
            return dao.findSymbol(code);
        }
    }

    private void proces(String host, String ip, final Boolean isTwse, final Dao dao) throws IOException {
        final HttpGet httpget = new HttpGet(String.format("https://%s/isin/C_public.jsp?strMode=%d", ip, isTwse ? 2 : 4));
        System.out.printf("\033[92mhttps://%s/isin/C_public.jsp?strMode=%d\033[0m\n", ip, isTwse ? 2 : 4);
        httpget.addHeader("Host", host);
        httpget.addHeader("User-Agent", Hunter.USER_AGENT);
        httpget.addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
        httpget.addHeader("Accept-Language", "zh-TW,zh;q=0.8,en-US;q=0.5,en;q=0.3");
        httpget.addHeader("Accept-Encoding", "gzip, deflate");
        httpget.addHeader("Upgrade-Insecure-Requests", "1");
        httpget.addHeader("Cache-Control", "max-age=0");
        hc.execute(httpget, new ResponseHandler<Void>() {

            @Override
            public Void handleResponse(HttpResponse hr) throws ClientProtocolException, IOException {
                if (hr.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                    try {
                        DOMParser parser = new DOMParser();
                        parser.setProperty("http://cyberneko.org/html/properties/default-encoding", "Big5_HKSCS");
                        parser.setFeature("http://xml.org/sax/features/namespaces", false);
                        parser.setProperty("http://cyberneko.org/html/properties/names/elems", "lower");
                        parser.parse(new org.xml.sax.InputSource(hr.getEntity().getContent()));
                        Document doc = parser.getDocument();
                        XPathExpression expr = xpath.compile(".//TD[contains(text(),'" + (isTwse ? "上市" : "上櫃") + "')]/parent::TR");
                        Object result = expr.evaluate(doc, XPathConstants.NODESET);
                        NodeList trs = null;
                        if (result == null || (trs = (NodeList) result).getLength() == 0) {
                            throw new InvalidObjectException("無法取得股票代號: " + httpget.getURI().toString());
                        } else if (trs.getLength() > 0) {
                            List<String> codes = new ArrayList<>(trs.getLength() + (isTwse ? 3 : 1));
                            List<String> names = new ArrayList<>(trs.getLength() + (isTwse ? 3 : 1));
                            codes.add(isTwse ? "t00" : "o00");
                            names.add(isTwse ? "加權股價指數" : "櫃買指數");
                            if (isTwse) {
                                codes.add("0050");
                                names.add("元大台灣50");
                                codes.add("FRMSA");
                                names.add("寶島股價指數");
                            }
                            for (int idx = 0; idx < trs.getLength(); idx++) {
                                Node tr = trs.item(idx);
                                Node td = tr.getFirstChild();
                                String[] cn = td.getFirstChild().getNodeValue().trim().split("[　]", 2);
                                if (cn.length > 1) {
                                    cn[1] = cn[1].replaceAll("　", "").trim();
                                    if (!codes.contains(cn[0])) {
                                        codes.add(cn[0]);
                                        names.add(cn[1]);
                                    }
                                }
                            }
                            if (codes.isEmpty()) {
                                log.log(Level.INFO, "沒有新增{0}資料", isTwse ? "上市" : "上櫃");
                            } else {

                                dao.insertStocks(codes, names, isTwse ? 0 : 1);
                                log.log(Level.INFO, "新增{0}{1}筆資料", new Object[]{isTwse ? "上市" : "上櫃", String.format("%,d", codes.size())});
                            }
                        }

                    } catch (SAXException | XPathExpressionException ex) {
                        log.log(Level.SEVERE, ex.getMessage(), ex);
                    }
                } else {
                    throw new HttpResponseException(hr.getStatusLine().getStatusCode(), hr.getStatusLine().getReasonPhrase());
                }
                return null;
            }
        });
    }

    @Override
    public void run() {
        String httpHost = "isin.twse.com.tw";
        String host = httpHost;
        try (Dao dao = new DBI(ds).open(Dao.class)) {
            proces(httpHost, host, true, dao);
            proces(httpHost, host, false, dao);
        } catch (IOException ex) {
            log.log(Level.SEVERE, ex.getMessage(), ex);
        }
    }

    public interface Dao extends AutoCloseable {

        @SqlQuery("SELECT name FROM sqlite_master WHERE type='table' and name = 'stock'")
        String stockTabName();

        @SqlUpdate("CREATE TABLE stock(code TEXT,name TEXT NOT NULL,clazz INTEGER NOT NULL,PRIMARY KEY (code))")
        void createTable();

        @SqlUpdate("delete from stock")
        int removeAll();

        @SqlBatch("insert into stock(code,name,clazz) values( :code , :name , :clazz)")
        void insertStocks(@Bind("code") List<String> codes, @Bind("name") List<String> names,
                @Bind("clazz") int clazz);

        @SqlQuery("select case clazz when 0 then 'tse_' else 'otc_' end||code||'.tw' "
                + " from stock where code= :code")
        String findSymbol(@Bind("code") String code);

        void close();
    }
}

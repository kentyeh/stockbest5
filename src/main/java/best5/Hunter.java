package best5;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;
import net.minidev.json.JSONValue;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.cyberneko.html.parsers.DOMParser;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author kent
 */
public class Hunter {

    private static final Logger log = Logger.getLogger(Hunter.class.getName());
    public static final String USER_AGENT = "Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0";
    private CloseableHttpClient hc = null;
    private final XPath xpath = XPathFactory.newInstance().newXPath();
    private static final Pattern pErase = Pattern.compile("'(\\w+)'");
    private static final Pattern pJHead = Pattern.compile("item\\.(\\w+)\\s*=\\s*item\\.(\\w+)");
    private static final Pattern p5 = Pattern.compile("(\\d*(\\.\\d+)?)_?");
    private static final String MISTWSE = "mis.twse.com.tw";
    private static String host = MISTWSE;

    /*static {
     try {
     host = InetAddress.getByName(MISTWSE).getHostAddress();
     } catch (UnknownHostException ex) {
     log.log(Level.WARNING, "無法取得{0}網址", MISTWSE);
     host = MISTWSE;
     }
     }*/
    public Hunter(CloseableHttpClient hc) {
        this.hc = hc;
    }

    private String getSymbol(String code) {
        String symbol = CodeDB.getInstance(hc).findSymbol(code);
        if (symbol == null || symbol.isEmpty()) {
            log.log(Level.WARNING, "找不到股票代號: {0}", code);
        }
        return symbol;
    }

    public boolean mustPath(final String url) throws IOException {
        HttpGet headget = new HttpGet(url);
        headget.addHeader("Host", MISTWSE);
        headget.addHeader("User-Agent", USER_AGENT);
        headget.addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
        headget.addHeader("Accept-Language", "zh-TW,zh;q=0.8,en-US;q=0.5,en;q=0.3");
        headget.addHeader("Accept-Encoding", "gzip, deflate");
        headget.addHeader("X-Requested-With", "XMLHttpRequest");
        headget.addHeader("Referer", "http://mis.twse.com.tw/stock/fibest.jsp?lang=zh_tw");
        return (hc.execute(headget, new ResponseHandler<Boolean>() {
            @Override
            public Boolean handleResponse(HttpResponse hr) throws ClientProtocolException, IOException {
                if (hr.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                    EntityUtils.consume(hr.getEntity());
                    return true;
                } else {
                    Logger.getLogger(Hunter.class.getName()).log(Level.WARNING, "[{0}]{1} {2}", new Object[]{hr.getStatusLine().getStatusCode(), hr.getStatusLine().getReasonPhrase(), url});
                    return false;
                }
            }
        }));
    }

    public boolean mustPass() throws IOException {
        long now = new Date().getTime();
        return mustPath(String.format("https://%s/stock/api/getStockInfo.jsp?ex_ch=tse_t00.tw%%7cotc_o00.tw%%7ctse_FRMSA.tw&json=1&delay=0&_=%d", host, now));//&& mustPath(String.format("http://%s/stock/data/futures_side.txt?_=%d", host, now))
        //&& mustPath(String.format("http://%s/stock/api//getShowChart.jsp?_=%d", host, now))
        //&& mustPath(String.format("http://%s/stock/api/getStock.jsp?ch=1101.tw&json=1&_=%d", host, now));
    }

    public List<Item> hunt(String[] codes) throws IOException {
        HttpGet headget = new HttpGet(String.format("https://%s/stock/fibest.jsp?lang=zh_tw", host));
        headget.addHeader("Host", MISTWSE);
        headget.addHeader("User-Agent", USER_AGENT);
        headget.addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
        headget.addHeader("Accept-Language", "zh-TW,zh;q=0.8,en-US;q=0.5,en;q=0.3");
        headget.addHeader("Accept-Encoding", "gzip, deflate");
        headget.addHeader("Upgrade-Insecure-Requests", "1");
        final Map<String, String[]> mapper = new HashMap<>();
        if (hc.execute(headget, new ResponseHandler<Boolean>() {
            @Override
            public Boolean handleResponse(HttpResponse hr) throws ClientProtocolException, IOException {
                if (hr.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                    DOMParser parser = new DOMParser();
                    try {
                        parser.setProperty("http://cyberneko.org/html/properties/default-encoding", "UTF8");
                        parser.setFeature("http://xml.org/sax/features/namespaces", false);
                        parser.setProperty("http://cyberneko.org/html/properties/names/elems", "lower");
                        parser.parse(new org.xml.sax.InputSource(hr.getEntity().getContent()));
                        Document doc = parser.getDocument();
                        XPathExpression expr = xpath.compile(".//SCRIPT");
                        NodeList scripts = (NodeList) expr.evaluate(doc.getDocumentElement(), XPathConstants.NODESET);
                        for (int i = 0; i < scripts.getLength(); i++) {
                            String scriptlet = getNodeText(scripts.item(i));
                            if (scriptlet.contains("function rA")) {
                                int sidx = scriptlet.indexOf("{");
                                int eidx = scriptlet.lastIndexOf("}");
                                String rA = "";
                                for (String line : scriptlet.substring(sidx + 1, eidx).split(";")) {
                                    Matcher mj = pJHead.matcher(line);
                                    if (line.contains("new RegExp(")) {
                                        Matcher m = pErase.matcher(line);
                                        if (m.find()) {
                                            rA = m.group(1);
                                        }
                                    } else if (mj.find() && !mj.group(1).equals(mj.group(2))) {
                                        if (rA.isEmpty()) {
                                            throw new IllegalArgumentException(mj.group(1) + "發生匹配錯誤:\n"
                                                    + scriptlet);
                                        }
                                        mapper.put(mj.group(1), new String[]{mj.group(2), rA});
                                        rA = "";
                                    }
                                }
                            }
                        }
                        return true;
                    } catch (SAXException | XPathExpressionException ex) {
                        Logger.getLogger(Hunter.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
                    }
                } else {
                    throw new HttpResponseException(hr.getStatusLine().getStatusCode(), hr.getStatusLine().getReasonPhrase());
                }
                return false;
            }
        })) {
            List<String> urls = new ArrayList<>();
            long ts = new Date().getTime();
            for (String code : codes) {
                String sym = getSymbol(code);
                if (sym != null && !sym.isEmpty()) {
                    String url = String.valueOf("https://" + host + "/stock/api/getStockInfo.jsp?ex_ch=" + sym + "&json=1&delay=0&_=") + Long.toString(new Date().getTime());
                    urls.add(url);
                } else {
                    Logger.getLogger(Hunter.class.getName()).log(Level.SEVERE, "找不到[{0}]的股票代號", new Object[]{code});
                }
            }
            List<Item> res = new ArrayList<>();
            for (String url : urls) {
                HttpGet best5 = new HttpGet(url);
                best5.addHeader("Host", MISTWSE);
                best5.addHeader("User-Agent", USER_AGENT);
                best5.addHeader("Accept", "application/json, text/javascript, */*; q=0.01");
                best5.addHeader("Accept-Language", "zh-TW,zh;q=0.8,en-US;q=0.5,en;q=0.3");
                best5.addHeader("Accept-Encoding", "gzip, deflate");
                best5.addHeader("X-Requested-With", "XMLHttpRequest");
                headget.addHeader("Referer", String.format("https://%s/stock/fibest.jsp?stock=&lang=zh_tw", host));
                JSONArray ja = null;
                ja = hc.execute(best5, new ResponseHandler<JSONArray>() {

                    @Override
                    public JSONArray handleResponse(HttpResponse hr) throws ClientProtocolException, IOException {
                        if (HttpStatus.SC_OK == hr.getStatusLine().getStatusCode()) {
                            String jsons = EntityUtils.toString(hr.getEntity());
                            if (jsons == null || jsons.isEmpty()) {
                                return null;
                            }
                            try {
                                JSONArray ja = (JSONArray) ((JSONObject) JSONValue.parse(jsons)).get("msgArray");
                                if (ja == null) {
                                    throw new IllegalArgumentException("無法解析:" + jsons);
                                } else {
                                    return ja;
                                }
                            } catch (ClassCastException ex) {
                                throw new IllegalArgumentException("無法解析:\"" + jsons + "\"");
                            }
                        } else {
                            throw new HttpResponseException(hr.getStatusLine().getStatusCode(), hr.getStatusLine().getReasonPhrase());
                        }
                    }

                });
                if (ja != null && !ja.isEmpty()) {
                    for (int i = 0; i < ja.size(); i++) {
                        res.add(parse((JSONObject) ja.get(i), mapper));
                    }
                }
            }
            return res;
        }
        /*StringBuilder sb = new StringBuilder("https://").append(host).append("/stock/api/getStockInfo.jsp?ex_ch=");
         int i = 0;
         for (String code : codes) {
         String sym = getSymbol(code);
         if (sym != null && !sym.isEmpty() && i++ > 0) {
         sb.append("%7c");
         }
         sb.append(sym);
         }
         sb.append("&json=1&delay=0&_=").append(new Date().getTime());
         HttpGet best5 = new HttpGet(sb.toString());
         best5.addHeader("Host", MISTWSE);
         best5.addHeader("User-Agent", USER_AGENT);*/
        //best5.addHeader("Accept", "application/json, text/javascript, */*; q=0.01");
            /*best5.addHeader("Accept-Language", "zh-TW,zh;q=0.8,en-US;q=0.5,en;q=0.3");
         best5.addHeader("Accept-Encoding", "gzip, deflate");
         best5.addHeader("X-Requested-With", "XMLHttpRequest");
         headget.addHeader("Referer", String.format("http://%s/stock/fibest.jsp?stock=&lang=zh_tw", host));
         JSONArray ja = null;
         ja = hc.execute(best5, new ResponseHandler<JSONArray>() {

         @Override
         public JSONArray handleResponse(HttpResponse hr) throws ClientProtocolException, IOException {
         if (HttpStatus.SC_OK == hr.getStatusLine().getStatusCode()) {
         String jsons = EntityUtils.toString(hr.getEntity());
         if (jsons == null || jsons.isEmpty()) {
         return null;
         }
         try {
         JSONArray ja = (JSONArray) ((JSONObject) JSONValue.parse(jsons)).get("msgArray");
         if (ja == null) {
         throw new IllegalArgumentException("無法解析:" + jsons);
         } else {
         return ja;
         }
         } catch (ClassCastException ex) {
         throw new IllegalArgumentException("無法解析:\"" + jsons + "\"");
         }
         } else {
         throw new HttpResponseException(hr.getStatusLine().getStatusCode(), hr.getStatusLine().getReasonPhrase());
         }
         }

         });
         if (ja == null || ja.isEmpty()) {
         return Collections.EMPTY_LIST;
         } else {
         List<Item> res = new ArrayList<>(ja.size());
         for (i = 0; i < ja.size(); i++) {
         res.add(parse((JSONObject) ja.get(i), mapper));
         }
         return res;
         }
         }*/
        return Collections.EMPTY_LIST;
    }

    private String process(JSONObject jo, Map<String, String[]> mapper, String key) {
        String[] maps = mapper.get(key);
        if (maps == null || maps[0] == null || maps[0].isEmpty()) {
            String sv = jo.getAsString(key);
            return sv == null || sv.trim().isEmpty() ? "" : sv;
        }
        String sv = jo.getAsString(maps[0]);
        if (sv == null || sv.isEmpty()) {
            return "";
        } else if (maps[1] == null || maps[1].isEmpty()) {
            return sv;
        } else {
            return sv.replaceAll(maps[1], "");
        }
    }

    private Item parse(JSONObject jo, Map<String, String[]> mapper) {
        Item item = new Item(jo.getAsString("c"), jo.getAsString("n"));
        String sv = process(jo, mapper, "o");
        if (!sv.isEmpty() && !"-".equals(sv)) {
            item.setOp(new BigDecimal(sv));
        }
        sv = process(jo, mapper, "z");
        if (!sv.isEmpty()) {
            item.setEp("-".equals(sv) ? BigDecimal.ZERO : new BigDecimal(sv));
        }
        sv = process(jo, mapper, "y");
        if (!sv.isEmpty()) {
            item.setYp(new BigDecimal(sv));
        }
        sv = process(jo, mapper, "h");
        if (!sv.isEmpty() && !"-".equals(sv)) {
            item.setHp(new BigDecimal(sv));
        }
        sv = process(jo, mapper, "l");
        if (!sv.isEmpty() && !"-".equals(sv)) {
            item.setLp(new BigDecimal(sv));
        }
        sv = process(jo, mapper, "u");
        if (!sv.isEmpty()) {
            item.setUp(new BigDecimal(sv));
        }
        sv = process(jo, mapper, "w");
        if (!sv.isEmpty()) {
            item.setDp(new BigDecimal(sv));
        }
        sv = process(jo, mapper, "s");
        if (!sv.isEmpty()) {
            item.setTv("-".equals(sv) ? 0 : Integer.valueOf(sv, 10));
        }
        sv = process(jo, mapper, "v");
        if (!sv.isEmpty()) {
            item.setSv(Integer.valueOf(sv, 10));
        }
        sv = process(jo, mapper, "b");
        if (!sv.isEmpty()) {
            Matcher m = p5.matcher(sv);
            int i = 0;
            while (m.find() && !m.group(1).isEmpty()) {
                item.setB5p(i++, new BigDecimal(m.group(1)));
            }
        }
        sv = process(jo, mapper, "g");
        if (!sv.isEmpty()) {
            Matcher m = p5.matcher(sv);
            int i = 0;
            while (m.find() && !m.group(1).isEmpty()) {
                item.setB5v(i++, Integer.valueOf(m.group(1)));
            }
        }
        sv = process(jo, mapper, "a");
        if (!sv.isEmpty()) {
            Matcher m = p5.matcher(sv);
            int i = 0;
            while (m.find() && !m.group(1).isEmpty()) {
                item.setS5p(i++, new BigDecimal(m.group(1)));
            }
        }
        sv = process(jo, mapper, "f");
        if (!sv.isEmpty()) {
            Matcher m = p5.matcher(sv);
            int i = 0;
            while (m.find() && !m.group(1).isEmpty()) {
                item.setS5v(i++, Integer.valueOf(m.group(1)));
            }
        }
        item.setStime(jo.getAsString("t"));
        return item;
    }

    private String getNodeText(Node node) throws XPathExpressionException {
        XPathExpression expr = xpath.compile(".//text()");
        NodeList txtnodes = (NodeList) expr.evaluate(node, XPathConstants.NODESET);
        StringBuilder sb = new StringBuilder().append("");
        for (int i = 0; i < txtnodes.getLength(); i++) {
            String txt = txtnodes.item(i).getNodeValue();
            txt = txt == null ? "" : txt.trim();
            if (!txt.isEmpty()) {
                if (i > 0) {
                    sb.append('\n');
                }
                sb.append(txt);
            }
        }
        return sb.toString();
    }
}

package best5;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Kent Yeh
 */
public class Item implements Serializable {

    private static final long serialVersionUID = 1773908204310668457L;
    private static final Pattern pHan = Pattern.compile("\\p{InCJKUnifiedIdeographs}");
    private static final Pattern pTailZero = Pattern.compile("\\.\\d*(0+)$");
    public static final boolean isWindows = System.getProperty("os.name").contains("Windows");
    private static final String grayBC = isWindows ? "" : "\033[100m";
    private static final String redC = isWindows ? "" : "\033[91;100m";
    private static final String greenC = isWindows ? "" : "\033[92;100m";
    private static final String normalC = isWindows ? "" : "\033[0m";
    private String code;
    private String name;

    private BigDecimal op = BigDecimal.ZERO; //開盤價
    private BigDecimal ep = BigDecimal.ZERO; //最近/後成交價
    private BigDecimal yp = BigDecimal.ZERO; //昨日收盤價
    private BigDecimal hp = BigDecimal.ZERO; //最高價
    private BigDecimal lp = BigDecimal.ZERO; //最低價
    private BigDecimal up = BigDecimal.ZERO; //漲停價
    private BigDecimal dp = BigDecimal.ZERO; //跌停價
    private int tv = 0; //當盤成交量
    private int sv = 0; //累積成交量
    private final BigDecimal[] s5p = new BigDecimal[5];//五檔賣價
    private final BigDecimal[] b5p = new BigDecimal[5];//五檔買價
    private final int[] s5v = new int[]{0, 0, 0, 0, 0};//五檔賣量
    private final int[] b5v = new int[]{0, 0, 0, 0, 0};//五檔買量

    private String stime = "";

    //<editor-fold defaultstate="collapsed" desc="屬性設定">
    public Item() {
        for (int i = 0; i < 5; i++) {
            s5p[i] = BigDecimal.ZERO;
            b5p[i] = BigDecimal.ZERO;
        }
    }

    public Item(String code, String name) {
        this();
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getOp() {
        return op;
    }

    public void setOp(BigDecimal op) {
        this.op = op == null ? BigDecimal.ZERO : op;
        this.op.setScale(2);
    }

    public BigDecimal getEp() {
        return ep;
    }

    public void setEp(BigDecimal ep) {
        this.ep = ep == null ? BigDecimal.ZERO : ep;
        this.ep.setScale(2);
    }

    public BigDecimal getYp() {
        return yp;
    }

    public void setYp(BigDecimal yp) {
        this.yp = yp == null ? BigDecimal.ZERO : yp;
        this.yp.setScale(2);
    }

    public BigDecimal getHp() {
        return hp;
    }

    public void setHp(BigDecimal hp) {
        this.hp = hp == null ? BigDecimal.ZERO : hp;
        this.hp.setScale(2);
    }

    public BigDecimal getLp() {
        return lp;
    }

    public void setLp(BigDecimal lp) {
        this.lp = lp == null ? BigDecimal.ZERO : lp;
        this.lp.setScale(2);
    }

    public BigDecimal getUp() {
        return up;
    }

    public void setUp(BigDecimal up) {
        this.up = up == null ? BigDecimal.ZERO : up;
        this.up.setScale(2);
    }

    public BigDecimal getDp() {
        return dp;
    }

    public void setDp(BigDecimal dp) {
        this.dp = dp == null ? BigDecimal.ZERO : dp;
        this.dp.setScale(2);
    }

    public int getTv() {
        return tv;
    }

    public void setTv(int tv) {
        this.tv = tv;
    }

    public int getSv() {
        return sv;
    }

    public void setSv(int sv) {
        this.sv = sv;
    }

    public BigDecimal getS5p(int i) {
        return i > -1 && i < 5 ? s5p[i] : BigDecimal.ZERO;
    }

    public void setS5p(int i, BigDecimal p) {
        if (p != null) {
            p.setScale(2);
        }
        s5p[i] = i > -1 && i < 5 && p != null && p.compareTo(BigDecimal.ZERO) > 0 ? p : null;
    }

    public BigDecimal getB5p(int i) {
        return i > -1 && i < 5 ? b5p[i] : BigDecimal.ZERO;
    }

    public void setB5p(int i, BigDecimal p) {
        if (p != null) {
            p.setScale(2);
        }
        b5p[i] = i > -1 && i < 5 && p != null && p.compareTo(BigDecimal.ZERO) > 0 ? p : null;
    }

    public int getS5v(int i) {
        return i > -1 && i < 5 ? s5v[i] : 0;
    }

    public void setS5v(int i, int p) {
        s5v[i] = i > -1 && i < 5 && p > 0 ? p : 0;
    }

    public int getB5v(int i) {
        return i > -1 && i < 5 ? b5v[i] : 0;
    }

    public void setB5v(int i, int p) {
        b5v[i] = i > -1 && i < 5 && p > 0 ? p : 0;
    }

    public String getStime() {
        return stime;
    }

    public void setStime(String stime) {
        this.stime = stime == null ? "" : stime;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 23 * hash + Objects.hashCode(this.code);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Item other = (Item) obj;
        return Objects.equals(this.code, other.code);
    }
    //</editor-fold>

    private int llen(String src) {
        int cnt = src == null || src.isEmpty() ? 0 : src.length();
        if (cnt > 0) {
            Matcher m = pHan.matcher(src);
            while (m.find()) {
                cnt++;
            }
        }
        return cnt;
    }

    private String rmTail0(String src) {
        src = src == null ? "" : src.trim();
        Matcher m = pTailZero.matcher(src.trim());
        src = m.find() ? src.replaceAll("0+$", "") : src;
        return src.endsWith(".") ? src.substring(0, src.length() - 1) : src;
    }

    private String alignL(String src, int len) {
        src = src == null ? "" : src.trim();
        int plen = llen(src);
        if (plen < len) {
            StringBuilder sb = new StringBuilder(src);
            for (int i = 0; i < len - plen; i++) {
                sb.append(" ");
            }
            return sb.toString();
        } else {
            StringBuilder sb = new StringBuilder();
            plen = 0;
            for (int i = 0; i < src.length(); i++) {
                String chr = src.substring(i, i + 1);
                boolean isHan = pHan.matcher(chr).matches();
                if (plen + (isHan ? 2 : 1) > len) {
                    if (plen < len) {
                        sb.append(" ");
                    }
                    break;
                }
                if (isHan && plen + (isHan ? 2 : 1) <= len) {
                    plen += isHan ? 2 : 1;
                    sb.append(chr);
                } else {
                    plen++;
                    sb.append(chr);
                }
            }
            return sb.toString();
        }
    }

    private String alignR(String src, int len) {
        src = src == null ? "" : src.trim();
        StringBuilder sb = new StringBuilder(len);
        int plen = llen(src);
        if (plen < len) {
            for (int i = 0; i < len - src.length(); i++) {
                sb.append(" ");
            }
            sb.append(src);
        } else {
            plen = 0;
            for (int i = 0; i < src.length(); i++) {
                String chr = src.substring(i, i + 1);
                boolean isHan = pHan.matcher(chr).matches();
                if (plen + (isHan ? 2 : 1) > len) {
                    if (plen < len) {
                        sb.append(" ");
                    }
                    break;
                }
                if (isHan && plen + (isHan ? 2 : 1) <= len) {
                    plen += isHan ? 2 : 1;
                    sb.append(chr);
                } else {
                    plen++;
                    sb.append(chr);
                }
            }

        }
        return sb.toString();
    }

    private String colorTag(BigDecimal val) {
        return isWindows || val == null || val.equals(yp) ? grayBC : val.compareTo(yp) > 0 ? redC : greenC;
    }

    private String ns(BigDecimal bd) {
        return bd == null ? "" : bd.toString();
    }

    @Override
    public String toString() {
        if (BigDecimal.ZERO.equals(ep)) {
            ep = getB5p(0);
        }
        StringBuilder sb = new StringBuilder(grayBC)
                .append(code)
                .append(alignL("", 8 - code.length()))
                .append(stime)
                .append(alignL("", 8 - stime.length())).append(" ► ")
                .append(alignR(BigDecimal.ZERO.equals(ep)
                                        ? rmTail0(ns(yp))
                                        : rmTail0(ns(ep)), 5)).append("(");
        if (ep == null || ep.equals(yp) || sv == 0) {
            sb.append("     0,    0%)");
        } else if (ep.compareTo(yp) > 0) {
            sb.append(redC).append(ep.equals(up) ? "▲" : "△")
                    .append(alignR(rmTail0(ep.subtract(yp).toString()), 5))
                    .append(",").append(alignR(rmTail0(ep.subtract(yp).multiply(BigDecimal.valueOf(100l)).
                                            divide(yp, BigDecimal.ROUND_HALF_UP).toString()), 5)).append("%")
                    .append(grayBC).append(")");
        } else {
            sb.append(greenC).append(ep.equals(up) ? "▼" : "▽")
                    .append(alignR(rmTail0(yp.subtract(ep).toString()), 5))
                    .append(",").append(alignR((yp.subtract(ep).multiply(BigDecimal.valueOf(100l)).
                                    divide(yp, BigDecimal.ROUND_HALF_UP).toString()), 5)).append("%")
                    .append(grayBC).append(")");
        }
        int headWidth = 13;
        sb.append(redC).append(" ▲ ").append(grayBC).append(alignR(alignR(rmTail0(ns(up)), 5), 5))
                .append(",").append(greenC).append("▼ ").append(grayBC).append(alignR(rmTail0(ns(dp)), 5))
                .append(",開:").append(colorTag(op)).append(alignR(rmTail0(ns(op)), 5)).append(grayBC)
                .append(",高:").append(colorTag(hp)).append(alignR(rmTail0(ns(hp)), 5)).append(grayBC)
                .append(",低:").append(colorTag(lp)).append(alignR(rmTail0(ns(lp)), 5)).append(normalC)
                .append("\n");
        if ("t00,o00,FRMSA".contains(code)) {
            sb.append(name).append(" 累計成交：").append(String.format("%,d", sv));
        } else {
            String width5 = "      ";
            for (int i = 0; i < 5; i++) {
                if (i == 0) {
                    String p1 = getB5p(i) == null ? "" : rmTail0(getB5p(i).toString());
                    p1 = p1.length() > 6 ? alignR(p1, 6) : p1;
                    sb.append(alignL(name, headWidth + 6 - (p1.isEmpty() ? 0 : p1.length() + 1))).append(" ").append(p1);
                } else {
                    sb.append(",").append(getB5p(i) == null ? width5 : alignR(rmTail0(getB5p(i).toString()), 6));
                }
            }
            sb.append("|");
            for (int i = 0; i < 5; i++) {
                sb.append(i == 0 ? "" : ",")
                        .append(getS5p(i) == null ? width5 : alignR(rmTail0(getS5p(i).toString()), 6));
            }

            sb.append("\n");
            for (int i = 0; i < 5; i++) {
                if (i == 0) {
                    String v1 = rmTail0(String.valueOf(getB5v(i)));
                    v1 = v1.length() > 6 ? alignR(v1, 6) : v1;
                    int availWitdth = headWidth + 6 - (v1.isEmpty() ? 0 : v1.length() + 1);
                    String vol = String.format("%,d", tv) + "/" + String.format("%,d", sv);
                    if (vol.length() > availWitdth) {
                        vol = tv + "/" + sv;
                    }
                    if (vol.length() > availWitdth) {
                        vol = String.format("%,d", tv) + "/" + String.format("%,dK", sv / 1000);
                    }
                    if (vol.length() > availWitdth) {
                        vol = tv + "/" + String.format("%dK", sv / 1000);
                    }
                    vol += (vol.length() > availWitdth ? "" : alignL("", availWitdth - vol.length()));
                    sb.append(vol).append(" ").append(v1);
                } else {
                    sb.append(",").append(getB5p(i) == null ? width5 : alignR(String.format(getB5v(i) > 99999 ? "%06d" : "%,d", getB5v(i)), 6));
                }
            }
            sb.append("|");
            for (int i = 0; i < 5; i++) {
                sb.append(i == 0 ? "" : ",")
                        .append(getS5p(i) == null ? width5 : alignR(String.format(getS5v(i) > 99999 ? "%6d" : "%,6d", getS5v(i)), 6));
            }
        }
        return sb.toString();
    }

}
